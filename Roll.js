import React, { Component } from 'react';
import { StyleSheet, Text, View } from "react-native";

class Roll extends Component {
  state = {
    diceRollLabelText: 'd%s-rolls: '
  }

  renderLabel = () => {
    return this.state.diceRollLabelText.replace('%s', this.props.diceRollNumber.diceType);
  }

  render = () => {
    if (this.props.diceRollNumber
      && this.props.diceRollNumber.diceType
      && this.props.diceRollNumber.numbers
      && this.props.diceRollNumber.numbers.length > 0) {

      return (
        <View style={styles.rolls}>
          <Text style={styles.label}>{this.renderLabel()}</Text>
          <Text style={styles.text}>{this.props.diceRollNumber.numbers.join(', ')}</Text>
        </View>
      )
    }
    return <Text></Text>
  }
}

const mainButtonColor = "cornflowerblue";
const controlColor = "darkcyan";

const styles = StyleSheet.create({
  view: { backgroundColor: "black" },
  table: { marginTop: 20 },
  tableBorder: { borderWidth: 2, borderColor: controlColor },
  wrapper: { flexDirection: "row" },
  label: { color: "#fff" },
  text: { color: controlColor },
  rolls: { marginLeft: 10, flex: 0, flexDirection: "row" }
});

export default Roll;