import React, { Component } from 'react';
import { StyleSheet, Button, View, Text } from "react-native";
import Roll from './Roll';
import DiceRollNumber from './DiceRollNumber';

class Rolls extends Component {
  state = {
    rollAreaTitle: 'Rolls',
    rollAgainButtonText: 'Roll again'
  }

  renderRolls() {
    return this.props.diceConfigs
      .filter(diceConfig => diceConfig.diceCount > 0)
      .map(diceConfig => new DiceRollNumber(diceConfig))
      .map(this.renderRoll);
  }

  renderRoll(diceRollNumber) {
    return (
      <Roll
        key={'roll_' + diceRollNumber.diceType}
        diceRollNumber={diceRollNumber} />
    );
  }

  hasToRender() {
    return this.props.diceConfigs
      && this.props.diceConfigs
        .filter(diceConfig => diceConfig.diceCount > 0)
        .length > 0;
  }

  render = () => {
    if (this.hasToRender()) {
      return (
        <View>
          <Text style={styles.rollsTitle}>
            {this.state.rollAreaTitle}
          </Text>
          <View style={styles.rollArea}>
            {this.renderRolls()}
          </View>
          <Button
            onPress={this.props.onReRoll}
            title={this.state.rollAgainButtonText} />
        </View>
      );
    }
    return <Text></Text>
  }
}

const styles = StyleSheet.create({

  rollsTitle: {
    color: "#fff",
    fontSize: 20,
    textAlign: "center",
    marginTop: 20
  },
  rollArea: {
    marginBottom: 10
  }
});

export default Rolls;