import React, { Component } from 'react';
import { StyleSheet, Button, View, Text } from "react-native";
import DiceChooser from './DiceChooser';

class DiceChoosers extends Component {
  state = {
    showDiceChoosers: true,
    diceChooserTitle: 'Dice choosers',
    resetToZeroText: 'Reset all to zero'
  }

  renderDiceChoosers = () => {
    if (this.state.showDiceChoosers) {
      return (
        <View style={styles.diceChoosers}>
          <View style={styles.innerView}>
            {this.props.diceConfigs.map(this.renderDiceChooser)}
          </View>
        </View>
      )
    }
    return <Text></Text>
  }

  renderDiceChooser = (diceConfig) => {
    return (
      <DiceChooser key={'diceChooser_' + diceConfig.diceType}
        diceConfig={diceConfig}
        onAddDice={() => this.props.onAddDice(diceConfig.diceType)}
        onRemoveDice={() => this.props.onRemoveDice(diceConfig.diceType)} />
    );
  }

  toggleDiceChoosers = () => {
    this.setState({ showDiceChoosers: !this.state.showDiceChoosers });
  }

  renderResetButton = () => {
    if (this.state.showDiceChoosers && this.hasAtLeastOneDice()) {
      return <Button
        style={styles.button}
        onPress={this.props.onResetAllToZero}
        title={this.state.resetToZeroText} />
    }
    return <Text>aa</Text>
  }

  hasAtLeastOneDice = () => {
    return this.props.diceConfigs && this.props.diceConfigs
      .filter(diceConfig => diceConfig.diceCount > 0)
      .length > 0;
  }

  render() {
    if (this.props.diceConfigs) {
      return (
        <View>
          <Button onPress={this.toggleDiceChoosers} title={this.state.diceChooserTitle} />
          {this.renderDiceChoosers()}
          {this.renderResetButton()}
        </View>
      )
    }
    return <Text></Text>
  }
}

const controlsColor = "darkcyan";

const styles = StyleSheet.create({
  diceChoosers: {
    flex: 0,
    flexDirection: "row",
    width: "100%",
    marginBottom: 10,
    flex: 0,
    flexDirection: "column",
    alignItems: "center"
  },
  innerView: {
    flex: 0,
    flexDirection: "row",
  },
  button: {
    color: controlsColor
  }
});

export default DiceChoosers;