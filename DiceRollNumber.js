class DiceRollNumber {

  constructor(diceConfig) {
    this.diceType = diceConfig.diceType;
    this.numbers = this.randomNumbers(diceConfig);
  }

  randomNumbers(diceConfig) {
    const numbers = [];
    for (var i = 0; i < diceConfig.diceCount; i++) {
      numbers.push(this.randomNumber(diceConfig.diceType));
    }
    return numbers;
  }

  randomNumber(max) {
    return Math.floor(Math.random() * max) + 1;
  }
}

export default DiceRollNumber;