import React, { Component } from 'react';
import { StyleSheet, Button, View, Text } from "react-native";
import DiceChoosers from './DiceChoosers.js';
import Rolls from './Rolls';
import DiceConfig from './DiceConfig.js';

class App extends Component {
  state = {
    diceConfigs: [
      new DiceConfig(2, 5),
      new DiceConfig(4, 0),
      new DiceConfig(6, 2),
      new DiceConfig(8, 0),
      new DiceConfig(10, 3),
      new DiceConfig(12, 0),
      new DiceConfig(20, 1),
      new DiceConfig(100, 0)
    ]
  }

  handleAddDice = diceType => {
    this.state.diceConfigs
      .filter(diceConfig => diceConfig.diceType === diceType)
      .forEach(diceConfig => diceConfig.diceCount++);
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  handleRemoveDice = diceType => {
    this.state.diceConfigs
      .filter(diceConfig => diceConfig.diceType === diceType)
      .forEach(diceConfig => {
        if (diceConfig.diceCount > 0) {
          diceConfig.diceCount--;
        }
      });
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  handleResetAllToZero = () => {
    this.state.diceConfigs.forEach(diceConfig => diceConfig.diceCount = 0);
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  handleReRoll = () => {
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          <DiceChoosers
            diceConfigs={this.state.diceConfigs}
            onAddDice={this.handleAddDice}
            onRemoveDice={this.handleRemoveDice}
            onResetAllToZero={this.handleResetAllToZero}
          />
          <Rolls diceConfigs={this.state.diceConfigs} onReRoll={this.handleReRoll} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "black",
    alignSelf: "center",
    width: "100%",
    height: "100%",
    paddingTop: 30,
    paddingBottom: 30,
    paddingLeft: 10,
    paddingRight: 10,
    color: "green"
  },
  mainContainer: {
    backgroundColor: "#400",
    alignSelf: "center",
    width: "100%",
    height: "100%"
  },
  mainButton: {
    color: "cornflowerblue"
  }
});

export default App;