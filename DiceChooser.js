import React, { Component } from 'react';
import { StyleSheet, Button, Text, View } from "react-native";

class DiceChooser extends Component {

  render = () => {
    if (this.props.diceConfig) {
      return (
        <View style={styles.viewStyle}>
          <Text
            style={styles.type}>
            d{this.props.diceConfig.diceType}
          </Text>
          <Button
            style={styles.button}
            onPress={this.props.onAddDice}
            title='+' />
          <Text
            style={styles.counter}>
            {this.props.diceConfig.diceCount}
          </Text>
          <Button
            style={styles.button}
            onPress={this.props.onRemoveDice}
            title='-' />
        </View>
      );
    }
    return <Text></Text>
  }
}

const controlsColor = "darkcyan";

const styles = StyleSheet.create({
  viewStyle: {
    flex: 0,
    flexDirection: "column",
    alignItems: "center",
    borderRadius: 1,
    margin: 2,
    width: 35,
    borderColor: "#f00"
  },
  type: {
    color: controlsColor
  },
  counter: { color: controlsColor },
  button: {
    color: controlsColor
  }
});

export default DiceChooser;